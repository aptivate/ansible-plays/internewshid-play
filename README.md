[![pipeline status](https://git.coop/aptivate/ansible-plays/internewshid-play/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-plays/internewshid-play/commits/master)

# internewshid-play

The InternewsHID playbooks.

For deployment documentation, please see:

> https://projects.aptivate.org/projects/internewshid/wiki/Wiki#section-6
